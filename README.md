# Auto Package Linker

This small utility will link packages together that you are working on locally!

### Project Config

You can modify the linker via the projectConfig in autoLink.js. The project config should mirror
the structure of how you want your packages to link to your project.

#### projectConfig

Accepts an object with the following keys:

| key         | required  | type             | notes                                             |
|-------------|-----------|------------------|---------------------------------------------------|
| projectPath | true      | string           | Absolute path to project                          |
| packageName | true*     | string           | Name of package, *not required for base project   |
| linksTo     | false     | projectConfig[]  | Array of projects to link to                      |

Packages inside of the ```linksTo``` array can also have packages in a ```linksTo``` array to have multiple
levels of linking.

<strong>NOTE: </strong> the base project will not create a link nor build automatically

### Start Linker

From the root of this project, run ```npm start```. The utility will begin building and
linking projects together. 
